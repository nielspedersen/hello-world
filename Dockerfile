FROM openjdk:8-jre-alpine

COPY target/scala-2.11/*.jar /app.jar

CMD /usr/bin/java -jar /app.jar

