import org.scalatest.{ Matchers, WordSpec }
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server._
import Directives._

class MainSpec extends WordSpec with Matchers with ScalatestRouteTest {

  val smallRoute = {
    get {
      pathSingleSlash {
        complete {
          "Hello, world!"
        }
      }
    }
  }

  "The service" should {
    "return 'Hello, world!'" in {
      Get() ~> smallRoute ~> check {
        responseAs[String] shouldEqual "Hello, world!"
      }
    }
  }
}

