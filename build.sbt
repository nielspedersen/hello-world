enablePlugins(JavaAppPackaging)

scalaVersion := "2.12.3"

assemblyJarName in assembly := "akka-http-hello-world.jar"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.3",
  "com.typesafe.akka" %% "akka-stream" % "2.5.3",
  "com.typesafe.akka" %% "akka-http" % "10.0.9",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.9",
  "org.scalatest" %% "scalatest" % "3.0.1"  % "test"
)

